
let allTabs = document.querySelectorAll('[data-target]');
let tabText = document.querySelectorAll('[data-tab-content]');


allTabs.forEach(e =>{

    e.addEventListener('click',() => {
        allTabs.forEach(el =>{
            el.classList.remove('active');
        });

        let tabActive = document.getElementById(e.dataset.target);
        tabActive.classList.add('active');

        tabText.forEach(text => {
            text.classList.remove('active');
        });

        let textActive = document.getElementById(e.dataset.target.slice(1,e.dataset.target.length));
        textActive.classList.add('active');
    })
});

